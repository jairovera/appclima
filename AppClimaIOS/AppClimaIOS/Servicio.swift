//
//  Servicio.swift
//  AppClimaIOS
//
//  Created by Jairo Vera on 7/11/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import Foundation
class Servicio{
    func consultaPorCiudad(city:String, completion:@escaping (String)->()){
        
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=367ed9b4eeb27bafedd8ca6c97bb83c7"
        
        consulta(urlString: urlStr) { (weather,ciudad) in
            completion(weather)
        }
        
    }
    
    func consultaPorUbicacion(lat:Double, lon: Double, completion:@escaping (String,String)->()){
        
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=f4b14259b70ff9328f483a7729e0c980"
        
        consulta(urlString: urlStr) { (weather, ciudad) in
            completion(weather,ciudad)
        }
        
    }
    
    
    func consulta(urlString:String,completion:@escaping (String, String)->()){
        //escaping es salir al hilo principal
        //completion es identificador
        
        let url = URL(string:urlString)
        let request = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let _ = error {
                return
            }
            do{
                let weatherJson = try JSONSerialization.jsonObject(with: data!, options: [])
                let weatherDict = weatherJson as! NSDictionary
                guard let weatherKey = weatherDict["weather"] as? NSArray else {
                    completion("Ciudad no valida","Error")
                    return
                }
                let weather = weatherKey[0] as! NSDictionary
                completion("\(weather["description"] ?? "Error")" ,"\(weatherDict["name"] ?? "Error")")
                
            }catch {
                print("Error del JSON")
            }
            
        }
        
        task.resume()
        //URLSession es Singleton
        //.shared es instancia de URLSession configurada
    }
}

