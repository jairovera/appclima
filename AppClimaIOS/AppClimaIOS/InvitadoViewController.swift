//
//  InvitadoViewController.swift
//  AppClimaIOS
//
//  Created by Jairo Vera on 31/10/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import UIKit
import CoreLocation

class InvitadoViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var weatherLabel: UILabel!
    
    @IBOutlet weak var cityLabel: UILabel!
    let locationManager = CLLocationManager()
    var bandera = false
    
    //MARK:- ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
            
        }
        
        //consultarPorUbicación()
    }
    //fjghfhgf
    //MARK:- LocationManager Delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate
      //  if !bandera{
        consultarPorUbicacion(
            lat:(location?.latitude)!,
            lon:(location?.longitude)!
            )
        //    bandera = true
        //}
        locationManager.stopUpdatingLocation()
    }
    
    
    //MARK:- Actions
    private func consultarPorUbicacion (lat:Double, lon:Double) {
        
        let servicio = Servicio()
        servicio.consultaPorUbicacion(lat: lat, lon: lon) { (weather, ciudad) in
            DispatchQueue.main.async {
                self.weatherLabel.text=weather
                self.cityLabel.text=ciudad
                
            }
        }
      
    }

}
