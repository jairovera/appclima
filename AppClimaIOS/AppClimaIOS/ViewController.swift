//
//  ViewController.swift
//  AppClimaIOS
//
//  Created by Jairo Vera on 25/10/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

   
    
    //MARK:- Outlets
    
    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    //MARK:- ViewController Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        usernameTextField.text = ""
        passwordTextField.text = ""
        usernameTextField.becomeFirstResponder()
    }
    
    //MARK:- Actions
    
    @IBAction func entrarButtonPressed(_ sender: Any) {
        
        let user = usernameTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        switch(user,password){
        case ("jairo","jairo"):
            performSegue(withIdentifier: "ciudadSegue", sender: self)
        case ("jairo", _):
            print("contraseña incorrecta")
            mostrarAlerta(mensaje: "Password Malo")
        default:
            print("contraseña y usuario incorrecto")
            mostrarAlerta(mensaje: "usuario y password incorrecto")
        }
        
    }
    
    @IBAction func invitadoButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "invitadoSegue", sender: self)
    }
    
    private func mostrarAlerta(mensaje:String){
        let alertView = UIAlertController(title: "Error", message: mensaje, preferredStyle: .alert)
        
        let aceptar = UIAlertAction(title: "Aceptar", style: .default) { (action) in
            self.usernameTextField.text=""
            self.passwordTextField.text=""
        }
        
        alertView.addAction(aceptar)
        
        present(alertView, animated: true, completion: nil)
        
    }
}

