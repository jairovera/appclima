//
//  CiudadViewController.swift
//  AppClimaIOS
//
//  Created by Jairo Vera on 31/10/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import UIKit

class CiudadViewController: UIViewController {

    
    //MARK:- Outlets
    
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var weatherLabel: UILabel!
    
    //MARK:- Actions
    
    
    @IBAction func consultarButtonPressed(_ sender: Any) {
        //api 367ed9b4eeb27bafedd8ca6c97bb83c7
        
        let service = Servicio()
        service.consultaPorCiudad(city: cityTextField.text!) { (weather) in
            DispatchQueue.main.async {
                self.weatherLabel.text = weather
            }
        }
        
        
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
}
